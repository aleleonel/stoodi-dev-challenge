# PROCESSO DE SELEÇÃO DE ENGENHEIRO DE SOFTWARE

História 0:

BUG - a ordem das respostas da questão estão aparecendo embaralhadas para o usuário. 
Faça as respostas aparacerem na ordem correta para o usuário
('a', 'b', 'c', 'd' e 'e');

# História 1:
Crie uma estrutura de models para guardar a questão no banco de dados.

 - Nesse commit foi criado um link na pagina de resposta que direciona o usuário 
 para uma pagina que permita salvar o resultado de sua resposta. 
 obs - O Usuário terá de digitar manualmente sua resposta;
 
# História 2:
Faça com que a view pegue a questão no banco de dados.

- Nesse commit uma view simples pega do banco de dados a resposta da questao aproveito
para tambem listar o nome de quem respondeu e se ele acertou ou não;

#História 3:

Faça com que a view de correção use o banco de dados.

- nesse commit ao listar as respostas acessando o banco pode-se alterar/corrigir
os dados cadastrado
- o mesmo pode ser feito na listagem de usuarios

#História 5: Logar cada questão respondida com:
nesse commit uma listagem é apresentada com o nome do usuário que respondeu
o resultado escolhido e se a resposta esta certa ou não

- data em que a questão foi respondida; eu entendi mas não consegui fazer
- alternativa escolhida; eu entendi, mas não consegui fazer
- se a alternativa está correta ou não. Eu tratei a logica no html embora essa 
nao seja a melhor pratica.

# História 6:
Crie um sistema simples de cadastro e login para o site.
O cadastro deve ficar na url "/cadastro/" e o login em "/login/".

- Nesse commit foi criado um sistema de cadastro simples (http://127.0.0.1:8000/cadastro/),
e uma view que lista todos os usuários cadastrados (http://127.0.0.1:8000/person_list/)
 para cadastrar é nescessário estar logado, acessando a url (http://127.0.0.1:8000/login/)
- mas conforme solicitado mesmo usuários não logados ainda podem responder as questoẽs.
- foi criada uma view simples de confirmação.