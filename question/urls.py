#coding: utf8

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.question, name='question'),
    url(r'^home/$', views.home, name='home'),
    url(r'^resposta/$', views.question_answer, name='question_answer'),
    url(r'^salva/$', views.salve_to_db, name='salve_to_db'),
    url(r'^person_list/$', views.person_list, name='person_list'),
    url(r'^question_list/$', views.question_list, name='question_list'),
    url(r'^update_question_list/(?P<id>\d+)/$', views.question_update, name='update_question_list'),
    url(r'^cadastro/$', views.cadastro, name='cadastro'),
    url(r'^update/(?P<id>\d+)/$', views.cadastro_update, name='cadastro_update'),



]


