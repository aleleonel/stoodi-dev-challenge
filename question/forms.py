from django.forms import ModelForm
from .models import TAnswer, TCadastro


class CadastroForm(ModelForm):
    class Meta:
        model = TCadastro
        fields = ['nome', 'sobrenome', 'idade', 'profissao', 'email']


class TAnswerForm(ModelForm):
    class Meta:
        model = TAnswer
        fields = ['answer_quest', 'resposta', 'cadastro']
