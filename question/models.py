from django.db import models


class TCadastro(models.Model):
    nome = models.CharField(max_length=50)
    sobrenome = models.CharField(max_length=50)
    idade = models.CharField(max_length=3)
    profissao = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return self.nome


class TAnswer(models.Model):
    answer_quest = models.CharField(max_length=15)
    resposta = models.BooleanField(default=False, blank=True)
    cadastro = models.ForeignKey(TCadastro, on_delete=models.CASCADE)

    def __str__(self):
        return self.answer_quest
