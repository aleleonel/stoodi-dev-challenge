#coding: utf8
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import TAnswer, TCadastro
from .forms import TAnswerForm, CadastroForm


def home(request):
    return render(request, 'cadastros/home.html')


def question(request):
    text = 'Quanto é 2^5?'
    answers = {
        'a': '0',
        'b': '2',
        'c': '16',
        'd': '32',
        'e': '128',
    }

    context = {
        'question_text': text,
        'answers': answers,
    }

    return render(request, 'question/question.html', context=context)


@login_required
def question_answer(request):
    answer = request.POST.get('answer', 'z')

    is_correct = answer == 'd'

    context = {
        'is_correct': is_correct,

    }

    return render(request, 'question/answer.html', context=context)


def salve_to_db(request):
    form = TAnswerForm(request.POST)
    if form.is_valid():
        form.save()

        return redirect('home')

    return render(request, 'question/answer_form.html', {'form': form})


def question_update(request, id):
    answer = get_object_or_404(TAnswer, pk=id)
    form = TAnswerForm(request.POST or None, request.FILES or None, instance=answer)

    if form.is_valid():
        form.save()
        return redirect('question_list')
    return render(request, 'question/answer_form.html', {'form': form})


def person_list(request):
    persons = TCadastro.objects.all()
    return render(request, 'cadastros/person_list.html', {'persons': persons})


def question_list(request):
    questions = TAnswer.objects.all()
    return render(request, 'question/question_list.html', {'questions': questions})


@login_required
def cadastro(request):
    form = CadastroForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
        return redirect('person_list')

    return render(request, 'cadastros/cadastro_form.html', {'form': form})


def cadastro_update(request, id):
    persons = get_object_or_404(TCadastro, pk=id)
    form = CadastroForm(request.POST or None, request.FILES or None, instance=persons)

    if form.is_valid():
        form.save()
        return redirect('person_list')
    return render(request, 'cadastros/cadastro_form.html', {'form': form})


